import React, {Component} from 'react';
import {TextInput, View} from 'react-native';
import styles from './index.style';


export default class InputField extends Component {
	render() {
		return (
			<View style={styles.card}>
				<TextInput
					ref={input => {
						this.textInput = input;
					}}
					style={styles.input}
					placeholder={this.props.placeholder}
					placeholderTextColor={'#999'}
					returnKeyType={'done'}
					autoCorrect={false}
					clearButtonMode={'always'}
					blurOnSubmit={false}
				/>
			</View>
		);
	}
}
