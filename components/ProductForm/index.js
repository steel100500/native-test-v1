import React, {Component} from 'react';
import {View, Image, Text} from 'react-native';
import InputField from './InputField';
import upload from '../../assets/upload.png'

import styles from './index.style';


export default class ProductForm extends Component {
	render() {
		return (
			<View style={styles.card}>
				<InputField placeholder='Add Title'/>
				<InputField placeholder='Category'/>
				<InputField placeholder='Article Name'/>
        <Image source={require('./upload.png')} />
			</View>
		);
	}
}
